import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.166.240.59:8085/')

WebUI.setText(findTestObject('Testcase_10/input_user_username'), username)

WebUI.setText(findTestObject('Testcase_10/input_user_password'), password)

WebUI.click(findTestObject('Object Repository/Testcase_10/button_Login'))

WebUI.click(findTestObject('Testcase_10/a_TotalTransaction_navbar'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Testcase_10/td_transactionId_result'), transactionId)

WebUI.verifyElementText(findTestObject('Testcase_10/td_product'), products)

WebUI.verifyElementText(findTestObject('Testcase_10/td_amount_result'), amount)

WebUI.verifyElementText(findTestObject('Testcase_10/p_Totalprice_result'), totalPrice)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//*[@id="add-row"]/div/table/tbody'))

'To locate rows of table it will Capture all the rows avilable in the table'
List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())

'Compare the value'
WebUI.verifyEqual(2, Rows.size())

WebUI.closeBrowser()

