import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.166.240.59:8085/')

WebUI.setText(findTestObject('Testcase_6/input_user_username'), username)

WebUI.setText(findTestObject('Testcase_6/input_user_password'), password)

WebUI.click(findTestObject('Object Repository/Testcase_6/button_Login'))

WebUI.verifyElementText(findTestObject('Testcase_6/h5_Garden_result'), productName1)

WebUI.verifyElementText(findTestObject('Testcase_6/h5_Banana_result'), productName2)

WebUI.verifyElementText(findTestObject('Testcase_6/h5_Orange_result'), productName3)

WebUI.verifyElementText(findTestObject('Testcase_6/h5_Papaya_result'), ProductName4)

WebUI.verifyElementText(findTestObject('Testcase_6/h5_Rambutan_result'), ProductName5)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('/html/body/app-root/app-product-list/div/div[2]/div'))

List<WebElement> Rows = Table.findElements(By.className('farmer-card'))

println('No. of rows: ' + Rows.size())

'Compare the value'
WebUI.verifyEqual(5, Rows.size())

WebUI.closeBrowser()

