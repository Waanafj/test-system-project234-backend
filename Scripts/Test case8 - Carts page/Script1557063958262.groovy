import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.166.240.59:8085/')

WebUI.setText(findTestObject('Testcase_8/input_username'), username)

WebUI.setText(findTestObject('Testcase_8/input_password'), password)

WebUI.click(findTestObject('Testcase_8/button_Login'))

WebUI.click(findTestObject('Testcase_8/button_add to cart'))

WebUI.click(findTestObject('Testcase_8/a_user_Carts_navbar'))

WebUI.verifyElementText(findTestObject('Testcase_8/td_productName_result'), productName)

WebUI.verifyElementText(findTestObject('Testcase_8/td_priceProduct_result'), productPrice)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//*[@id="add-row"]/div/table/tbody/tr/td[3]'))

Table.findElement(By.tagName('input')).clear()

Table.findElement(By.tagName('input')).sendKeys(inputAmount)

WebUI.verifyElementText(findTestObject('Testcase_8/p_Totalprice_result'), totalPrice)

WebUI.click(findTestObject('Object Repository/Testcase_8/button_confirm'))

WebUI.closeBrowser()

