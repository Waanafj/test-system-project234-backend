<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d955fba8-665c-43d6-af6f-c0434d5be992</testSuiteGuid>
   <testCaseLink>
      <guid>26943d92-03e0-4d39-99b4-d0d19e6ccc0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case1-Open the web site</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fd69103-f6d7-445e-a221-a1283fb24d7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case2-Admin and User login to the system</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>70c51b5c-c8a9-4731-8e45-21e29bb7f3d0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data for Test case2</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>70c51b5c-c8a9-4731-8e45-21e29bb7f3d0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>b5d041bc-b21d-4d37-8815-93b5764581fc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>70c51b5c-c8a9-4731-8e45-21e29bb7f3d0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>55b642e6-39aa-4136-94f2-eafe40d8c040</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>70c51b5c-c8a9-4731-8e45-21e29bb7f3d0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>page1</value>
         <variableId>c7518f53-91a8-4897-8a8f-4b9e087f9df9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>70c51b5c-c8a9-4731-8e45-21e29bb7f3d0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>page2</value>
         <variableId>54a14d46-bdf3-46b3-b40d-4c039a298f51</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dcad309e-0153-47af-b3c4-86812e720323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case5-Validate password empty</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7ad6b809-af84-4caa-b217-d80b976e3912</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data for Test case5</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>7ad6b809-af84-4caa-b217-d80b976e3912</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>751f1b2b-7e24-4d9a-bba5-c75b00ec3fc8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cd37f3c6-0f44-4ddc-8d5f-a03086296074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case3-Validate username and password incorrect</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>78476bea-83c8-4d44-89a3-4165aaed664d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data for Test case3</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>78476bea-83c8-4d44-89a3-4165aaed664d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>db667900-de55-473c-b1ff-72b5f041048d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>78476bea-83c8-4d44-89a3-4165aaed664d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>0dc5dc31-e4a9-4bd9-aece-cf49df783492</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4c804611-8237-4daf-adc7-7926740593f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case4-Vadidate username empty</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cc41563e-7480-4938-8eb8-bf4d123db9fc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data for Test case4</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>cc41563e-7480-4938-8eb8-bf4d123db9fc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>b47e1a8c-9521-4e77-a2ef-67936b1a40bd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ea61222d-fae8-47ac-919a-70775bc06569</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case6 - Show all avilable products</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bdb9bc6a-a0da-4077-b501-628937a578cd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 6</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>5edf9b8e-1722-4adb-b199-b17c09c35045</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>cc80fcb4-a6e2-4e23-9c49-c48e1b1a215b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productName1</value>
         <variableId>3e409b3c-7094-4774-8db9-0a713c827c49</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productName2</value>
         <variableId>fa948239-cf10-4fd1-9bc6-4c88d051d0ed</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productName3</value>
         <variableId>a11b55c8-fd45-4458-8c09-d39d52320058</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productName4</value>
         <variableId>1f183609-e3d4-4529-b60f-a6dd391383e4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bdb9bc6a-a0da-4077-b501-628937a578cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productName5</value>
         <variableId>1e4e3acf-331c-4d42-8477-0aef40171181</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ba8ccbb9-544b-43c0-ad92-40e0866de3e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case7 - Update number when product added</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f967424c-587b-4812-864f-f3e9c32c0fb2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 7</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f967424c-587b-4812-864f-f3e9c32c0fb2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>c7a8c7d7-3baf-489e-9836-eb1137ab11be</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f967424c-587b-4812-864f-f3e9c32c0fb2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>9e212424-21af-464e-9fe3-9ae4b62392b8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f967424c-587b-4812-864f-f3e9c32c0fb2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>numberAdded</value>
         <variableId>c6d15174-a0da-4003-a58b-9cb490d0cb6d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>01a39a64-6f51-40d5-9835-598a0b44d13f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case8 - Carts page</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0bec7e74-1d92-47b7-8016-ea71a35c596f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 8</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>13aa715f-5934-492e-b8ca-d1cceff297d8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>76279310-19b9-49d1-8ae1-c93b1705afd9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productName</value>
         <variableId>0e64d73a-f505-4f25-851f-092f029c892b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>productPrice</value>
         <variableId>fd0c140f-78a8-4c5a-9c0f-edb9a804938d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>inputAmount</value>
         <variableId>373f3e14-ddae-441e-9f63-82fe0a0a7d5d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>totalPrice</value>
         <variableId>966a7990-7285-498d-8f96-6344a64eac66</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0bec7e74-1d92-47b7-8016-ea71a35c596f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>confirmMessagge</value>
         <variableId>4621509e-9c65-46eb-b340-31e0641fba09</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>68d56d29-5363-49db-bc6f-34fa10bf7c55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case9 - User logout</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ef43837c-24cf-4825-b5c4-9794c3b6ee01</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 9</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>ef43837c-24cf-4825-b5c4-9794c3b6ee01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>a7c5c1dd-09b0-4f35-a8c3-68d80ba6be04</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ef43837c-24cf-4825-b5c4-9794c3b6ee01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>233d5e16-9710-49d0-ad6d-68a7b197d0ab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ef43837c-24cf-4825-b5c4-9794c3b6ee01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>loginKey</value>
         <variableId>9e04718a-1179-476a-9b47-e47491cc4bfe</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8ba9c2ee-64cf-4d09-bd53-b80c07ab114b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case10 - shown summaries of all carts</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>206a1ea5-4300-463a-a868-0a5a01bd3870</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 10</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>206a1ea5-4300-463a-a868-0a5a01bd3870</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>usename</value>
         <variableId>9b1e6f5d-5289-4074-aef5-8647a35ea801</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>206a1ea5-4300-463a-a868-0a5a01bd3870</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>09c181ab-6a7d-499f-a289-5d0feafd33ef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>206a1ea5-4300-463a-a868-0a5a01bd3870</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>transactionId</value>
         <variableId>26106037-ca3c-4ef3-9ee4-5e1280041a69</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>206a1ea5-4300-463a-a868-0a5a01bd3870</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>products</value>
         <variableId>0c0557b8-e64f-4272-a24d-89d5a63f3099</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>206a1ea5-4300-463a-a868-0a5a01bd3870</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>amount</value>
         <variableId>50ad15cd-2538-4bc2-aa89-f1cb174a45b1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>206a1ea5-4300-463a-a868-0a5a01bd3870</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>totalPrice</value>
         <variableId>c09b43e2-14fa-477d-83b3-26aa23446ddb</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
