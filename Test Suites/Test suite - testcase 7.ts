<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suite - testcase 7</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>be232dad-9611-4c7f-94e7-1f421aae5dad</testSuiteGuid>
   <testCaseLink>
      <guid>3e2e1e86-f5d6-46e6-b574-30cd76ae00c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case7 - Update number when product added</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d6f4cf52-4e86-4476-be59-1c7e98aae66b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 7</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>d6f4cf52-4e86-4476-be59-1c7e98aae66b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>c7a8c7d7-3baf-489e-9836-eb1137ab11be</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d6f4cf52-4e86-4476-be59-1c7e98aae66b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>9e212424-21af-464e-9fe3-9ae4b62392b8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d6f4cf52-4e86-4476-be59-1c7e98aae66b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>numberAdded</value>
         <variableId>c6d15174-a0da-4003-a58b-9cb490d0cb6d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
