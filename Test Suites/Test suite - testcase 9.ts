<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suite - testcase 9</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>02f314db-c2ca-444c-9945-257054ab9c70</testSuiteGuid>
   <testCaseLink>
      <guid>3f1ac904-2801-4b68-9985-617da2c99a64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case9 - User logout</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8b13d856-3d31-4219-af73-b2901d2319a6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test data - testcase 9</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>8b13d856-3d31-4219-af73-b2901d2319a6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>a7c5c1dd-09b0-4f35-a8c3-68d80ba6be04</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8b13d856-3d31-4219-af73-b2901d2319a6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>233d5e16-9710-49d0-ad6d-68a7b197d0ab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8b13d856-3d31-4219-af73-b2901d2319a6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>loginKey</value>
         <variableId>9e04718a-1179-476a-9b47-e47491cc4bfe</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
